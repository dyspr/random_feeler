var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var numOfCircles = 20
var maxSize = 0.07
var dimension = 11
var initSize = 0.07
var angleArray = create2DArray(dimension, dimension + 2, 0, false)
var frame = 0

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var j = 0; j < dimension; j++) {
    for (var k = 0; k < dimension + 2; k++) {
      push()
      translate((j - Math.floor(dimension * 0.5)) * initSize * boardSize + pow(-1, ((k + 1) % 2)) * boardSize * initSize * 0.25, (k - Math.floor((dimension + 2) * 0.5)) * initSize * boardSize * sqrt(3) * 0.5)
      for (var i = 0; i < numOfCircles; i++) {
        push()
        translate(windowWidth * 0.5 + (i / numOfCircles) * 0.3 * boardSize * maxSize * sin(frame + angleArray[j][k][0]), windowHeight * 0.5 + (i / numOfCircles) * 0.3 * boardSize * maxSize * cos(frame + angleArray[j][k][1]))
        fill(255 * (i / numOfCircles))
        noStroke()
        ellipse(0, 0, boardSize * maxSize * (1 - (i / numOfCircles)))
        pop()
      }
      pop()
    }
  }

  frame += deltaTime * 0.001
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = [Math.random() * 2 * Math.PI, Math.random() * 2 * Math.PI]
      }
    }
    array[i] = columns
  }
  return array
}
